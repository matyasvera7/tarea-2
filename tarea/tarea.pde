boolean boton = false;
float a=250;
int b=0;
int c=0;
PImage photo;  //imagen para la alerta.
import processing.sound.*;
SoundFile stop; //sonido para la alerta.


void setup () 
{
size(900,900);
background(248, 215, 114);
photo = loadImage("Mano.png");
stop = new SoundFile(this,"incorrecto-bocina-.mp3");
image(photo, 0, 0);

}

void draw() {
background(248, 215, 114);
fill(255);
rect(200,100,500,300);


//Velocimetro
line(250,125,650,125);//linea superior
line(250,125,250,300);//linea lateral izquierda
line(650,125,650,300);//linea lateral derecha
line(290,125,290,200);//10 km
line(330,125,330,200);//20 km
line(370,125,370,200);//30 km
line(410,125,410,200);//40 km
line(450,125,450,200);//50 km
line(490,125,490,200);//60 km
line(530,125,530,200);//70 km
line(570,125,570,200);//80 km
line(610,125,610,200);//90 km

//Textos
PFont font1;
font1 = loadFont("Consolas-12.vlw");//Se carga la letra
  textFont(font1, 15);
  fill(0);
  
text("Km/h",430, 125);
text("10",280, 220);
text("0",245, 320);
text("10",280, 220);
text("20",320, 220);
text("30",360, 220);
text("40",400, 220);
text("50",440, 220);
text("60",480, 220);
text("70",520, 220);
text("80",560, 220);
text("90",600, 220);
text("100",635, 320);
text("Acelerar",100, 675);
text("frenar",720, 675);
fill( 9, 255, 0);
rect(100,700,100,100);

fill( 255, 0, 0 );
rect(720,700,100,100);

//Aceleracion
if ((mouseX>100)&&(mouseX<200)&&(mouseY>700)&&(mouseY<800 )&&(a<650)){
  if (a >649.1){
        a=a-0.999;
      }
  boton = true;
  a=a+1.5;
  if (b < 1){
  c = millis();
  }
b++;
  text("Tiempo ="+((millis()-c)/1000)+" [s]",400,790);
     if (((millis()-c)/1000)>=10){    //condicion para que funcione la alerta.
      image(photo, 350, 400,200,200);
      if (!stop.isPlaying()){
            stop.play();
          }
     }
  }else{
    c=0;
    b=0;
}
 
 
//Freno
if ((mouseX>720)&&(mouseX<820)&&(mouseY>700)&&(mouseY<800 )&&(a>=250)){
  boton = true;
  a=a-1;
  
}
if ((a>250)&&(a<=652)){
  a=a-1;
}

//indicador de velocidad
line(a,125,a,300);

//Luz de freno:
if ((mouseX > 720) && (mouseX < 820)){
  if ((mouseY > 700) && (mouseY < 800)){ 
     fill(255,0,0);
  rect(350,500,200,100);
  fill(0);
  text("Frenando",415,550);
  }
}

}
